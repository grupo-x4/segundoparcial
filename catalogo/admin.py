from django.contrib import admin
from .models import Cliente  # Asegúrate de importar el modelo Cliente desde el archivo correcto

class ClienteAdmin(admin.ModelAdmin):
    list_display = ['nombre', 'apellido', 'dni', 'direccion', 'fecha_nacimiento', 'edad']
    list_filter = ['edad']
    search_fields = ['nombre', 'apellido', 'dni']

admin.site.register(Cliente, ClienteAdmin)